package com.leetcode.stuyarithmetic;

/**
 * 二分查找
 *
 * @author Hornyun
 * @url <a href= "https://leetcode-cn.com/problems/binary-search/"></a>
 * @date 2021/9/17 11:11
 **/
public class BinarySearchMain
{
    /**
     * 二分查找 返回查找值的下标
     *
     * @param array  数组
     * @param target 查找值
     * @return 数组下标
     */
    public int search(int[] array, int target)
    {
        if (array == null || array.length == 0)
        {
            return -1;
        }

        int head = 0;
        int tail = array.length - 1;
        int mid = (head + tail) / 2;
        while (head != tail)
        {
            if (array[mid] > target)
            {

            }
            else if (array[mid] < target)
            {

            }
            else
            {
                return mid;
            }

            tail = mid;
            mid = (head + tail) / 2;
        }

        return 0;
    }
}
