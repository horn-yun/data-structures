package com.structure.tree;

import java.util.Stack;

/**
 * 二叉树前序遍历：前序遍历首先访问根节点，然后遍历左子树，最后遍历右子树。
 * <a href="https://leetcode-cn.com/leetbook/read/data-structure-binary-tree/">前序遍历 </a>
 *
 * @author Hornyun
 * @date 2021/11/8 9:46
 **/
public class PreorderTraversalMain
{
    public static void main(String[] args)
    {

    }

    /**
     * preorder traversal  by stack
     * @param treeNode binary tree
     */
    public void preorderTraversalByStack(TreeNode treeNode)
    {
        if (treeNode == null)
        {
            return;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        stack.push(treeNode);
        while (!stack.isEmpty())
        {
            TreeNode pop = stack.pop();
            System.out.println(pop.val + " ");
            if (pop.right != null)
            {
                stack.push(pop.right);
            }
            if (pop.left != null)
            {
                stack.push(pop.left);
            }
        }
    }

    /**
     * recursion of solution
     * @param treeNode binary tree
     */
    public void preorderTraversal(TreeNode treeNode)
    {
        if (treeNode != null)
        {
            System.out.println(treeNode.val +" ");
            if (treeNode.left != null)
            {
                preorderTraversal(treeNode.left);
            }
            if (treeNode.right != null)
            {
                preorderTraversal(treeNode.right);
            }
        }
    }

    /**
     * 二叉树数组生成
     *
     * @return binary tree
     */
    public static int[] generateTree(){
        return new int[]{1, 2, 34, 5, 6, 7};
    }
}
