package com.structure.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 中序遍历是先遍历左子树，然后访问根节点，然后遍历右子树。
 * traversal binary tree by inorder
 * <a href="https://leetcode-cn.com/leetbook/read/data-structure-binary-tree/">中序遍历 </a>
 *
 * @author Hornyun
 * @date 2021/11/8 10:33
 **/
public class InorderTraversalMain
{
    public List<Integer> inorderTraversal(TreeNode root)
    {
        List<Integer> result = new ArrayList<Integer>();
        inorderTraversalByRecursion(root, result);
        return result;
    }

    /**
     * binary tree inorder traversal by recursion
     *
     * @param node   tree
     * @param result inorder traversal num list
     */
    public void inorderTraversalByRecursion(TreeNode node, List<Integer> result)
    {
        if (node == null)
        {
            return;
        }

        inorderTraversalByRecursion(node.left, result);
        result.add(node.val);
        inorderTraversalByRecursion(node.right, result);
    }

    /**
     * binary tree inorder traversal by stack structure
     *
     * @param node tree node
     * @param result order list
     */
    public void inorderTraversalByStack(TreeNode node, List<Integer> result)
    {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        while (node != null || !stack.isEmpty())
        {
            while (node != null)
            {
                stack.push(node);
                node = node.left;
            }
            if (!stack.isEmpty())
            {
                node =  stack.pop();
                result.add(node.val);
                node = node.right;
            }
        }
    }
}
