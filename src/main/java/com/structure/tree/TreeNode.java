package com.structure.tree;

/**
 * binary tree
 * @author Hornyun
 * @date 2021/11/8 9:54
 **/
public class TreeNode
{
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode()
    {
    }

    public TreeNode(int val, TreeNode left, TreeNode right)
    {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
